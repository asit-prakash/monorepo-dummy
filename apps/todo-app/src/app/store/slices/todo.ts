import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import axios, { AxiosResponse } from 'axios';
import api from '../../utils/api';

export type Todo = {
  id: string;
  item: string;
};
export interface TodoState {
  todoItems: Todo[];
  loading: boolean;
}

const initialState: TodoState = {
  todoItems: [],
  loading: false,
};

export const getTodos = createAsyncThunk<
  AxiosResponse<any> | null,
  void,
  Record<string, unknown>
>('/todo/getTodos', async (_, thunkApi) => {
  const getTodosApi = api({
    method: 'GET',
    url: '/todoItems.json',
  });

  try {
    const response = await getTodosApi;
    return response;
  } catch (error) {
    console.log(error);
    if (axios.isAxiosError(error)) {
      if (error.response) return thunkApi.rejectWithValue(error.response);
    }
    return null;
  }
});

export const counterSlice = createSlice({
  name: 'todo',
  initialState,
  reducers: {
    addTodo: (state, action: PayloadAction<string>) => {
      const newTodo = {
        id: new Date().toISOString() + Math.random().toString(16).slice(2),
        item: action.payload,
      };
      state.todoItems.push(newTodo);
    },
    removeTodo: (state, action: PayloadAction<string>) => {
      const updatedTodoItems = state.todoItems.filter(
        (item) => item.id !== action.payload
      );
      state.todoItems = updatedTodoItems;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getTodos.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getTodos.fulfilled, (state, action) => {
      state.loading = false;
      if (action?.payload?.status === 200) {
        // const modifiedData: Todo[] = Object.keys(action.payload.data).map(
        //   (key) =>
        //     ({
        //       id: action?.payload?.data[key].id,
        //       item: action?.payload?.data[key].item,
        //     } as Todo)
        // );
        let modifiedData: Todo[] | [];

        if (action.payload.data !== null) {
          modifiedData = Object.keys(action.payload.data).map((key) => {
            const todos: Todo = {
              id: action?.payload?.data[key]?.id,
              item: action?.payload?.data[key]?.item,
            };
            return todos;
          });
        } else {
          modifiedData = [];
        }

        state.todoItems = modifiedData;
      }
    });
    builder.addCase(getTodos.rejected, (state) => {
      state.loading = false;
    });
  },
});

export const { addTodo, removeTodo } = counterSlice.actions;

export default counterSlice.reducer;
