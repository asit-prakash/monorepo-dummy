import { VFC, ReactNode } from "react";

const TodoItem: VFC<{ title: string; onRemove: () => void; children?: ReactNode | undefined }> = (props) => {
  const { title, onRemove, children } = props;

  return (
    <div data-testid="todo-item" onClick={onRemove}>
      {title}
      <div>{children}</div>
    </div>
  );
};

export default TodoItem;
