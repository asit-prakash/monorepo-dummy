import { useEffect } from "react";
import { useAppDispatch } from "../../hooks/useAppDispatch";
import { useAppSelector } from "../../hooks/useAppSelector";
import { getTodos } from "../../store/slices/todo";
import AddTodo from "./AddTodo/AddTodo";
import Todos from "./Todos/Todos";

const TodoApp = () => {
  const dispatch = useAppDispatch();
  // const [loading, setLoading] = useState<boolean>(true);
  const { loading } = useAppSelector((state) => state.todo);

  useEffect(() => {
    const getTodoItems = dispatch(getTodos());

    return () => {
      getTodoItems.abort();
    };
  }, [dispatch]);

  return (
    <>
      <AddTodo />
      {loading ? "Loading..." : <Todos />}
    </>
  );
};

export default TodoApp;
