// import { act, screen, waitForElementToBeRemoved } from "@testing-library/react";
// import { rest } from "msw";
// import { setupServer } from "msw/node";
// import userEvent from "@testing-library/user-event";
// import App from "../../App";
// import { renderWithRouter } from "../../utils/test";

// const server = setupServer(
//   rest.get("https://todo-react-ts-e671e-default-rtdb.firebaseio.com/todoItems.json", (req, res, ctx) => {
//     return res(
//       ctx.json({
//         "-Mjj2t5oj05kMmP0cwx2": {
//           id: "223",
//           item: "first",
//         },
//         "-Mjj31_f6dMrez4qfTO7": {
//           id: "22crf3",
//           item: "asit",
//         },
//       })
//     );
//   })
// );

// beforeAll(() => server.listen());
// afterEach(() => server.resetHandlers());
// afterAll(() => server.close());

// describe("Render TodoApp", () => {
//   test("Render add todo form ", () => {
//     act(() => {
//       renderWithRouter(<App />, { route: "/todo-app" });
//     });
//     expect(screen.getByTestId("todo-input")).toBeInTheDocument();
//     expect(screen.getByTestId("todo-input")).toHaveValue("");
//     expect(screen.getByTestId("add-todo")).toBeInTheDocument();
//   });

//   test("Render todo list with todo items", async () => {
//     act(() => {
//       renderWithRouter(<App />, { route: "/todo-app" });
//     });

//     await waitForElementToBeRemoved(() => screen.getByText("Loading..."));
//     const todoList = screen.getByTestId("todo-list");
//     expect(todoList).toBeInTheDocument();

//     const todoItems = screen.getAllByTestId("todo-item");
//     expect(todoItems).toHaveLength(2);
//     expect(todoItems[0]).toHaveTextContent("first");
//     expect(todoItems[1]).toHaveTextContent("asit");
//   });

//   test("render No todos when todo items are not present", async () => {
//     server.use(
//       rest.get("https://todo-react-ts-e671e-default-rtdb.firebaseio.com/todoItems.json", (req, res, ctx) => {
//         return res(ctx.json(null));
//       })
//     );
//     act(() => {
//       renderWithRouter(<App />, { route: "/todo-app" });
//     });

//     await waitForElementToBeRemoved(() => screen.getByText("Loading..."));
//     const todoList = screen.getByTestId("todo-list");
//     expect(todoList).toBeInTheDocument();
//     expect(screen.getByText("No todos")).toBeInTheDocument();
//   });

//   test("Add todo", async () => {
//     act(() => {
//       renderWithRouter(<App />, { route: "/todo-app" });
//     });

//     await waitForElementToBeRemoved(() => screen.getByText("Loading..."));

//     userEvent.type(screen.getByTestId("todo-input"), "learn react");
//     expect(screen.getByTestId("todo-input")).toHaveValue("learn react");

//     userEvent.click(screen.getByTestId("add-todo"));
//     expect(screen.getByTestId("todo-input")).toHaveValue("");
//     expect(screen.getByText("learn react")).toBeInTheDocument();
//   });

//   test("Remove todo", async () => {
//     act(() => {
//       renderWithRouter(<App />, { route: "/todo-app" });
//     });

//     await waitForElementToBeRemoved(() => screen.getByText("Loading..."));
//     const todoList = screen.getByTestId("todo-list");
//     expect(todoList).toBeInTheDocument();

//     const todoItems = screen.getAllByTestId("todo-item");
//     expect(todoItems).toHaveLength(2);

//     userEvent.click(todoItems[0]);
//     expect(todoItems[0]).not.toBeInTheDocument();
//     userEvent.click(todoItems[1]);
//     expect(todoItems[1]).not.toBeInTheDocument();

//     expect(screen.getByText("No todos")).toBeInTheDocument();
//   });
// });
