import { ChangeEvent, FormEvent, useState } from "react";
import { useAppDispatch } from "../../../hooks/useAppDispatch";
import { addTodo } from "../../../store/slices/todo";

const AddTodo = () => {
  // const [todo, setTodo] = useState<string | undefined>(undefined);
  const [todo, setTodo] = useState<string>("");

  const dispatch = useAppDispatch();

  const todoChangeHandler = (event: ChangeEvent<HTMLInputElement>): void => {
    const { target } = event;
    setTodo(target.value);
  };

  const addTodoHandler = (event: FormEvent): void => {
    event.preventDefault();
    dispatch(addTodo(todo));
    setTodo("");
  };

  return (
    <div>
      <form data-testid="add-todo-form" onSubmit={addTodoHandler}>
        <input
          data-testid="todo-input"
          type="text"
          placeholder="Enter todo"
          value={todo}
          onChange={todoChangeHandler}
        />
        <button data-testid="add-todo" type="submit">
          Add
        </button>
      </form>
    </div>
  );
};

export default AddTodo;
