import { useAppDispatch } from "../../../hooks/useAppDispatch";
import { useAppSelector } from "../../../hooks/useAppSelector";
import { removeTodo } from "../../../store/slices/todo";
import TodoItem from "../TodoItem/TodoItem";

const Todos = () => {
  const { todoItems } = useAppSelector((state) => state.todo);
  const dispatch = useAppDispatch();

  const removeTodoHandler = (todoId: string) => (): void => {
    dispatch(removeTodo(todoId));
  };
  return (
    <div data-testid='todo-list'>
      {todoItems.length ? (
        todoItems.map((item) => (
          <TodoItem key={item.id} title={item.item} onRemove={removeTodoHandler(item.id)}>
            <hr />
          </TodoItem>
        ))
      ) : (
        <p>No todos</p>
      )}
    </div>
  );
};

export default Todos;
