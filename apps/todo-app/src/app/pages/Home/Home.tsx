import { PrimaryButton } from '@alpha-monorepoo/components/ui';

const Home = () => {
  return (
    <div>
      Home page
      <PrimaryButton title="Click me" onClick={() => {
        //
      }} />
    </div>
  );
};

export default Home;
