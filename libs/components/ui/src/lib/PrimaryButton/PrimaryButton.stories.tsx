import { Story, Meta } from '@storybook/react';
import  PrimaryButton, { PrimaryButtonProps }  from './PrimaryButton';

export default {
  component: PrimaryButton,
  title: 'Button',
} as Meta;

const Template: Story<PrimaryButtonProps> = (args) => <PrimaryButton {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  title: 'Primary',
  onClick: () => {
    //
  },
};
